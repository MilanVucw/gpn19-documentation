image::https://gitlab.com/stoeps/gpn19-documentation/badges/master/pipeline.svg[]

= Documentation with any Editor

Files for my GPN 19 talk.

== If you want to use this, just clone the repository:

[source]
----
git clone https://gitlab.com/stoeps/gpn19-documentation.git
cd gpn19-documentation
git submodule sync --recursive
git submodule update --init --recursive
----

Now create a new upstream and sync to your very own repository.
I registered a scheduler and set a variable `$BUILDCONTAINER=true`, this will create the docker image.
The docker image needs to be present, before the first conversion runs, or the pipeline will show errors.

If you don't want to publish your files in Gitlab pages, rename the `public` folder in the repository root and delete the `pages:` part from `.gitlab-ci.yml`!

== ToDo

* Upload artifacts to
** Connections
* Confluence

== Use Cases

Edit documents in browser, tablet or mobile. After commit and push the pipeline will create all files we changed.
