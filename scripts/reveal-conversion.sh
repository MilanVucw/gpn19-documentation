#!/usr/bin/env bash
#
# Author:       Christoph Stoettner

# Create presentation
for i in /documents/*.adoc
do
  asciidoctor-revealjs \
      -r asciidoctor-diagram \
      -r asciidoctor-mathematical \
      -a revealjsdir=https://cdnjs.cloudflare.com/ajax/libs/reveal.js/3.7.0 \
      "$i"
done

# Replace width
for i in /documents/*.html
do
  sed -i 's/960/1280/g' "$i"
done

