#!/usr/bin/env bash
#
# Author:       Christoph Stoettner
#
# Create presentation
for i in /documents/*.adoc
do
  asciidoctor \
      -r asciidoctor-diagram \
      -r asciidoctor-mathematical \
      "$i"
done

