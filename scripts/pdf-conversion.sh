#!/usr/bin/env bash
#
# Author:       Christoph Stoettner
#
for i in /documents/*.adoc
do
  asciidoctor \
      -r asciidoctor-pdf \
      -r asciidoctor-diagram \
      -r asciidoctor-mathematical \
      -b pdf \
      "$i"
done

