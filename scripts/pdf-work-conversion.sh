#!/usr/bin/env bash
#
# Author:       Christoph Stoettner
#
for i in /documents/*.adoc
do
  asciidoctor \
      -r asciidoctor-pdf \
      -r asciidoctor-diagram \
      -r asciidoctor-mathematical \
      -b pdf \
      -a pdf-stylesdir=/pdftheme \
      -a pdf-style=work \
      -a media=prepress \
      "$i"
done

